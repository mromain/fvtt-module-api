class APIModule {
    constructor() {
        this.socket = null;
    }

    // Utility function to parse URL parameters
    parseURLParams() {
        const params = {};
        try {
            let search = window.location.search;
            if (search.startsWith('?')) search = search.slice(1);
            for (let query of search.split("&")) {
                let [key, value] = query.split("=");
                value = decodeURIComponent(value);
                try {
                    params[key] = JSON.parse(value);
                } catch (err) {
                    params[key] = value;
                }
            }
        } catch (err) {
            this.error("Error parsing query string");
            return null;
        }
        return params;
    }

    // Function to handle errors and send a response
    error(message) {
        this.reply({ success: false, error: message });
    }

    // Function to send a response and close the socket
    reply(result) {
        const body = document.body;
        body.textContent = JSON.stringify(result);
        if (this.socket) {
            this.socket.close();
        }
    }

    // Function to process the incoming request
    async processRequest() {
        const params = this.parseURLParams();
        if (!params) return;

        // Validate required parameters
        if (!params.sessionId) return this.error("API use requires query string 'sessionId'");
        if (!params.name) return this.error("API use requires query string 'name'");

        // Extract additional arguments
        const args = [];
        for (let i = 0; i < 10; i++) {
            if (params[`arg${i}`] !== undefined) {
                args.push(params[`arg${i}`]);
            } else {
                break;
            }
        }

        console.log("Got request with params: ", params, args);

        // Connect to the game socket, passing the sessionId as an initial data packet
        this.socket = io.connect(window.location.origin, {
            'reconnection': false,
            query: { session: params.sessionId }
        });

        // Handle socket connection errors
        this.socket.on('connect_error', (err) => {
            this.error(`Socket connection error: ${err.message}`);
        });

        // Emit a socket event with the extracted parameters
        try {
            const result = await new Promise((resolve, reject) => {
                this.socket.emit(params.name, ...args, (...responseArgs) => {
                    resolve(responseArgs);
                });
            });
            this.reply({ query: params, result, success: true });
        } catch (error) {
            this.error(`Error during socket emission: ${error.message}`);
        }
    }
}

// Initialize and run the APIModule when the DOM is ready
const api = new APIModule();
window.addEventListener("DOMContentLoaded", () => api.processRequest());
