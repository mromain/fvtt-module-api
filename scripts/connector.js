// Ensure the module is registered once Foundry VTT is ready
Hooks.once("init", () => {
  console.log("Initializing Your Module");

  // Register socket communication handlers
  Hooks.once("socketlib.ready", () => {
    const socket = socketlib.registerModule("api");

    // Register a handler for the handleRequest event
    socket.register("handleRequest", async (sessionId, entityType, entityName, ...args) => {
      console.log("Received request with sessionId:", sessionId, "entityType:", entityType, "entityName:", entityName, "args:", args);

      let collection;
      switch (entityType) {
        case 'actor':
          collection = game.actors;
          break;
        case 'item':
          collection = game.items;
          break;
        case 'scene':
          collection = game.scenes;
          break;
        case 'journal':
          collection = game.journal;
          break;
        case 'table':
          collection = game.tables;
          break;
        case 'playlist':
          collection = game.playlists;
          break;
        case 'card':
          collection = game.cards;
          break;
        default:
          throw new Error(`Unknown entity type '${entityType}'`);
      }

      const document = collection.find(doc => doc.name === entityName);

      if (!document) {
        throw new Error(`Document with name '${entityName}' not found in ${entityType}`);
      }

      // Return the document data
      return document.data;
    });

    // Register a handler for the handleRequest event
    socket.register("testApi", async () => {
      console.log("Received request with sessionId:", sessionId, "entityType:", entityType, "entityName:", entityName, "args:", args);

      // Return the document data
      return game.actors;
    });
  });
});
